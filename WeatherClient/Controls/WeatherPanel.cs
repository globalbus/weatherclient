﻿using System;
using System.Linq;
using System.Windows.Forms;
using WeatherClient.SerializationObjects;
using System.Drawing;

namespace WeatherClient.Controls
{
    class WeatherPanel : Panel
    {
        private CityAndForecast data;
        private EventHandler<EventArgs> eventHandler; 
        private Font font;
        private Brush brush;
        private float scaling;
        public WeatherPanel() {
            eventHandler = new EventHandler<EventArgs>(data_updated);
            font = new Font(FontFamily.GenericMonospace, 10, FontStyle.Regular);
            
            brush = new SolidBrush(Color.Black);
            var platform = System.Environment.OSVersion.Platform;
            if(platform!=PlatformID.WinCE)//silly workaround for bug in dotnet cf GDI
                scaling = (float)96/72;
            else
                scaling =1;
        }
        public CityAndForecast Data
        {
            get { return data; }
            set
            {
                if(data!=null)
                    data.updated-=eventHandler;
                data = value;
                if(data!=null)
                    data.updated += eventHandler;
                this.Refresh();
            } 
        }
        void data_updated(object sender, EventArgs e)
        {
            this.Invoke(new Action(() => { this.Refresh(); }));
        }
        ~WeatherPanel()
        {

        }
        protected override void OnResize(EventArgs e)
        {
            this.Refresh();
            base.OnResize(e);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            if (data != null && data.Forecast != null)
            {
                ForecastItem forecast = data.Forecast.getFirst();
                var nextDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1, 12, 0, 0, DateTimeKind.Utc);
                if (forecast != null)
                {


                    var icon = Images.get(forecast.weather.Var);
                    float iconHeight = 0, iconWidth = 0;
                    if (icon != null)
                    {
                        e.Graphics.DrawImage(icon, 0, 0, new Rectangle(0, 0, icon.Height, icon.Width), GraphicsUnit.Pixel);
                        iconHeight = (icon.Height * scaling);
                        iconWidth = (icon.Width * scaling);
                    }
                    string now = "Right Now";
                    SizeF sz = e.Graphics.MeasureString(now, font);
                    e.Graphics.DrawString(now, font, brush, iconWidth, 0);
                    e.Graphics.DrawString("Temperature "+forecast.temperature.ToString(), font, brush, iconWidth, sz.Height);
                    e.Graphics.DrawString(String.Format("Wind {0} {1}", forecast.windDirection.code, forecast.windSpeed.Mps), font, brush, iconWidth, sz.Height*2);
                    if (iconWidth * 4 < this.Width)
                        iconWidth = this.Width / 4;
                    for (int i = 0; i < 4; i++)
                    {
                        var nextForecast = data.Forecast.list.FirstOrDefault(x => x.From >= nextDay);
                        if (nextForecast != null)
                        {
                            int move = (int)iconWidth * i;
                            if (move + iconWidth > this.Width)
                                break;
                            icon = Images.get(nextForecast.weather.Var);
                            string temp = nextForecast.temperature.ToString();
                            sz = e.Graphics.MeasureString(temp, font);
                            e.Graphics.DrawString(nextDay.DayOfWeek.ToString().Substring(0, 2), font, brush, move, this.Height - iconHeight - sz.Height * 2);
                            e.Graphics.DrawString(temp, font, brush, move + (iconWidth - sz.Width) / 2, this.Height - sz.Height);
                            if (icon != null)
                            {
                                e.Graphics.DrawImage(icon, move, (int)(this.Height - iconHeight - sz.Height), new Rectangle(0, 0, icon.Height, icon.Width), GraphicsUnit.Pixel);
                            }
                            nextDay = nextDay.AddDays(1);
                        }
                        else
                            break;
                    }
                }
            }
        }
    }
}
