﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    public class Lastupdate
    {
        private DateTime value;
        [XmlIgnore]
        public DateTime Value { get { return value; } set { this.value = value; } }
        [XmlAttribute("value")]
        public string FromString { get { return TimeConv.convert(value); } set { this.value = TimeConv.convert(value); } }

    }
}
