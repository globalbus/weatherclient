﻿using System;
using System.Globalization;

namespace WeatherClient.SerializationObjects
{
    class TimeConv
    {
        static string format = "yyyy-MM-ddTHH:mm:ss";
        public static DateTime convert(string time)
        {
            //2013-12-11T06:35:38
            return DateTime.ParseExact(time, format , CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
        }
        public static string convert(DateTime time)
        {
            //2013-12-11T06:35:38
            return time.ToUniversalTime().ToString(format);
        }
    }
}
