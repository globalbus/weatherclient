﻿using System;

namespace WeatherClient.SerializationObjects
{
    public class CityAndForecast
    {
        public City city;
        public DateTime lastUpdate;
        private ForecastResults forecast;
        public ForecastResults Forecast { get{ return forecast;
        }
            set { forecast = value;
            if(updated!=null)
                foreach (Delegate item in updated.GetInvocationList())
                {
                    item.Method.Invoke(item.Target, new object[]{this, new EventArgs()});
                }
            }
        }
        public override bool Equals(object obj)
        {
            CityAndForecast comp = obj as CityAndForecast;
            if (comp != null)
                return city.Equals(comp.city);
            return false;
        }
        public event EventHandler<EventArgs> updated;
        public override int GetHashCode()
        {
            return city.GetHashCode();
        }
        public override string ToString()
        {
            return city.ToString();
        }
    }
}
