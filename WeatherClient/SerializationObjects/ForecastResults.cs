﻿using System;
using System.Linq;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    [XmlRoot("weatherdata")]
    public class ForecastResults
    {
        private Sun sun;
        //[XmlElement("location")]
        //public City city;
        [XmlElement("sun")]
        public Sun Sun { get { return sun; } set { sun = value; } }
        public bool isNight()
        {
            var now = DateTime.Now.ToUniversalTime().TimeOfDay;
            if (Sun.Rise.TimeOfDay > now && Sun.Set.TimeOfDay < now)
                return false;
            else
                return true;
        }
        [XmlArrayItem("time")]
        [XmlArray("forecast")]
        public ForecastItem[] list;

        internal ForecastItem getFirst()
        {
            if (list == null || list.Length==0)
                return null;
            var now = DateTime.Now.ToUniversalTime();
            if (list[0].To > now)
                return list[0];
            return list.FirstOrDefault(x => x.isNow());
        }
    }
}
