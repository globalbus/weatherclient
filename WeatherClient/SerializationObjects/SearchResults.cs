﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    [XmlRoot("cities")]
    public class SearchResults
    {
        //[XmlElement]
        //public float calctime;
        [XmlElement]
        public int count;
        //[XmlElement]
        //public string mode;


        [XmlArrayItem("item")]
        [XmlArray]
        public SearchItem[] list;
    }
}
