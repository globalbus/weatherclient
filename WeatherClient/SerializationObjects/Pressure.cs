﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class Pressure
    {
        [XmlAttribute]
        public string unit;
        [XmlAttribute]
        public float value;
    }
}
