﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class Temperature
    {
        [XmlAttribute]
        public string unit;
        [XmlAttribute]
        public float value;
        [XmlAttribute]
        public float min;
        [XmlAttribute]
        public float max;
        public override string ToString()
        {
            if(unit.Equals("celsius"))
                return String.Format("{0:0.#}C", value);
            else if (unit.Equals("imperial"))
                return String.Format("{0:0.#}F", value);
            else
                return String.Format("{0:0.#}", value);
        }
    }
}
