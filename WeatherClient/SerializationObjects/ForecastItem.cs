﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    public class ForecastItem
    {
        private DateTime from;
        private DateTime to;
        [XmlElement("symbol")]
        public Weather weather;
        [XmlElement("windDirection")]
        public WindDirection windDirection;
        [XmlElement("windSpeed")]
        public WindSpeed windSpeed;
        [XmlElement("temperature")]
        public Temperature temperature;
        [XmlElement]
        public Humidity humidity;
        [XmlElement]
        public Pressure pressure;
        [XmlElement]
        public Clouds clouds;
        [XmlIgnore]
        public DateTime From { get { return from; } set { from = value; } }
        [XmlIgnore]
        public DateTime To { get { return to; } set { to = value; } }
        [XmlAttribute("from")]
        public string FromStr { get { return TimeConv.convert(from); } set { from = TimeConv.convert(value); } }
        [XmlAttribute("to")]
        public string ToStr { get { return TimeConv.convert(to); } set { to = TimeConv.convert(value); } }
        public bool isNow()
        {
            var now = DateTime.Now.ToUniversalTime();
            //var test1 = now.CompareTo(from);
            //var test2 =now.CompareTo(to);
            if (now.CompareTo(from) > 0 && now.CompareTo(to) < 0)
                return true;
            return false;
        }

    }
}
