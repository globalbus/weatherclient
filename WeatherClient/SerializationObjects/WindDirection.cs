﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable]
    public class WindDirection
    {
        [XmlAttribute]
        public string name;
        private float value;
        [XmlAttribute]
        public string code;

        [XmlAttribute("deg")]
        public float Deg { get { return value; } set { this.value = value; } }
        [XmlAttribute("value")]
        public float Value { get { return value; } set { this.value = value; } }
    }
}
