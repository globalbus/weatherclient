﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class SearchItem
    {
        [XmlElement]
        public City city;
        //[XmlElement]
        //public Temperature temperature;
        //[XmlElement]
        //public Humidity humidity;
        //[XmlElement]
       // public Pressure pressure;
        //[XmlElement]
        //public Wind wind;
        //[XmlElement]
        //public Clouds clouds;
        //[XmlElement]
        //public Weather weather;
        [XmlElement]
        public Lastupdate lastupdate;
        public override string ToString()
        {
            return String.Format("{0},{1} - {2}",city.Name, city.Country, city.Coord);
        }
    }
}