﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class Humidity
    {
        [XmlAttribute]
        public string unit;
        [XmlAttribute]
        public float value;
    }
}
