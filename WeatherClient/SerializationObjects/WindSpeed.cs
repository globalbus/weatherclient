﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable]
    public class WindSpeed
    {
        [XmlAttribute]
        public string name;
        private float value;
        [XmlAttribute("mps")]
        public float Mps { get { return value; } set { this.value = value; } }
        //[XmlAttribute("value")]
        //public float Value { get { return value; } set { this.value = value; } }
    }
}
