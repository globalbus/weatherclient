﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class Coord
    {
        private float lat;
        private float lon;
        [XmlAttribute("lon")]
        public float Lon { get { return lon; } set { lon = value; } }
        [XmlAttribute("lat")]
        public float Lat { get { return lat; } set { lat = value; } }
        [XmlAttribute("latitude")]
        public float Latitude { get { return lat; } set { lat = value; } }
        [XmlAttribute("longitude")]
        public float Longitude { get { return lon; } set { lon = value; } }

        public override string ToString()
        {
            string lon;
            if (this.lon >= 0)
                lon = String.Format("E{0}", this.lon);
            else
                lon = String.Format("W{0}", -this.lon);
            string lat;
            if (this.lat >= 0)
                lat = String.Format("N{0}", this.lat);
            else
                lat = String.Format("S{0}", -this.lat);
            return String.Format("{0}, {1}", lon, lat);
        }
    }
}
