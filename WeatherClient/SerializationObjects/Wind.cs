﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable]
    public class Wind
    {
        [XmlElement]
        public WindDirection direction;
        [XmlElement]
        public WindSpeed speed;
    }
}
