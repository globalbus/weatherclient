﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    public class Sun
    {
        private DateTime rise;
        private DateTime set;
        [XmlIgnore]
        public DateTime Rise { get { return rise; } set { rise = value; } }
        [XmlIgnore]
        public DateTime Set { get { return set; } set { set = value; } }
        [XmlAttribute("rise")]
        public string RiseString { get { return TimeConv.convert(rise); } set { rise = TimeConv.convert(value); } }
        [XmlAttribute("set")]
        public string SetString { get { return TimeConv.convert(set); } set { set = TimeConv.convert(value); } }

    }
}
