﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]
    public class Weather
    {
        [XmlAttribute("number")]
        public int number;
        private string icon;
        private string name;

       [XmlAttribute("var")]
        public string Var { get { return icon; } set { icon = value; } }
        //[XmlAttribute("icon")]
        //public string Icon { get { return icon; } set { icon = value; } }
        //[XmlAttribute("value")]
        //public string Value { get { return name; } set { name = value; } }
        [XmlAttribute("name")]
        public string Name { get { return name; } set { name = value; } }
    }
}
