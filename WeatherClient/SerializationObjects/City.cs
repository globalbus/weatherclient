﻿using System;
using System.Xml.Serialization;

namespace WeatherClient.SerializationObjects
{
    [Serializable()]

    public class City
    {
        public override bool Equals(object obj)
        {
            City comp = obj as City;
            if (comp != null)
                if (comp.id == this.id)
                    return true;
            return false;
        }
        public override int GetHashCode()
        {
            return id;
        }
        private string name;
        private int id;
        private string country;
        private Coord coord;
        //private Sun sun;
        //[XmlElement("name")]
        //public string Name2 { get { return name; } set { name = value; } }
        //[XmlElement("location")]
        //public Coord Location { get { return coord; } set { coord = value; } }
        [XmlAttribute("name")]
        public string Name { get { return name; } set { name = value; } }
        [XmlAttribute("id")]
        public int Id { get { return id; } set { id = value; } }
        [XmlElement("coord")]
        public Coord Coord { get { return coord; } set { coord=value; } }
        [XmlElement("country")]
        public string Country { get { return country; } set { country = value; } }
        //[XmlElement("sun")]
        //public Sun Sun { get { return sun; } set { sun = value; } }
        public override string ToString()
        {
            return String.Format("{0},{1} - {2}", name, country, coord);
        }
    }
}
