﻿using System;
using System.Windows.Forms;
using WeatherClient.SerializationObjects;

namespace WeatherClient
{
    public partial class SearchForm : Form
    {
        WeatherApiWrapper weatherApi = new WeatherApiWrapper();

        public SearchForm()
        {
            InitializeComponent();
            menuItem1.Enabled = false;
            weatherApi.resultsAquired += this.searchResults;
        }
        ~SearchForm()
        {
            weatherApi.resultsAquired -= this.searchResults;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void clearComboBox()
        {
            comboBox1.Items.Clear();
            Cursor.Current = Cursors.Default;
        }
        private void searchResults(object sender, GenericEventArgs<SearchResults> e)
        {
            comboBox1.Invoke(new Action(clearComboBox));
            if (e.results != null&& e.results.count>0 )
            {
                var results = e.results.list;
                foreach (var item in results)
                {
                    comboBox1.Invoke(new Action<object>((it) => comboBox1.Items.Add(it)), new object[] {item.city});
                }
                comboBox1.Invoke(new Action(()=> comboBox1.SelectedIndex=0));
            }
            
        }
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter || e.KeyCode == Keys.MButton) && textBox1.Text.Length>0)
            {
                Cursor.Current = Cursors.WaitCursor;
                weatherApi.searchAsync(textBox1.Text);
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
                Settings.Instance.addCity(comboBox1.SelectedItem as City);
            this.Close();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
                menuItem1.Enabled = true;
        }
    }
}