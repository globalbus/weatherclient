﻿namespace WeatherClient
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu softKeyMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.softKeyMenu = new System.Windows.Forms.MainMenu();
            this.leftSoftButton = new System.Windows.Forms.MenuItem();
            this.rightSoftButton = new System.Windows.Forms.MenuItem();
            this.AddMenuItem = new System.Windows.Forms.MenuItem();
            this.RemoveMenuItem = new System.Windows.Forms.MenuItem();
            this.SettingsMenuItem = new System.Windows.Forms.MenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainPanel = new WeatherClient.Controls.WeatherPanel();
            this.closeItem = new System.Windows.Forms.MenuItem();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // softKeyMenu
            // 
            this.softKeyMenu.MenuItems.Add(this.leftSoftButton);
            this.softKeyMenu.MenuItems.Add(this.rightSoftButton);
            // 
            // leftSoftButton
            // 
            this.leftSoftButton.Text = "Update";
            this.leftSoftButton.Click += new System.EventHandler(this.update_Click);
            // 
            // rightSoftButton
            // 
            this.rightSoftButton.MenuItems.Add(this.AddMenuItem);
            this.rightSoftButton.MenuItems.Add(this.RemoveMenuItem);
            this.rightSoftButton.MenuItems.Add(this.SettingsMenuItem);
            this.rightSoftButton.MenuItems.Add(this.closeItem);
            this.rightSoftButton.Text = "More";
            // 
            // AddMenuItem
            // 
            this.AddMenuItem.Text = "Add";
            this.AddMenuItem.Click += new System.EventHandler(this.addNew_Click);
            // 
            // RemoveMenuItem
            // 
            this.RemoveMenuItem.Text = "Remove";
            this.RemoveMenuItem.Click += new System.EventHandler(this.removeCurrent_Click);
            // 
            // SettingsMenuItem
            // 
            this.SettingsMenuItem.Text = "Settings";
            this.SettingsMenuItem.Click += new System.EventHandler(this.openSettings_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.mainPanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(320, 186);
            // 
            // mainPanel
            // 
            this.mainPanel.Data = null;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(320, 186);
            // 
            // closeItem
            // 
            this.closeItem.Text = "Close";
            this.closeItem.Click += new System.EventHandler(this.closeItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(131F, 131F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(320, 186);
            this.Controls.Add(this.panel1);
            this.Menu = this.softKeyMenu;
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.Closed += new System.EventHandler(this.MainWindow_Closed);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem leftSoftButton;
        private System.Windows.Forms.MenuItem rightSoftButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuItem AddMenuItem;
        private System.Windows.Forms.MenuItem RemoveMenuItem;
        private System.Windows.Forms.MenuItem SettingsMenuItem;
        private WeatherClient.Controls.WeatherPanel mainPanel;
        private System.Windows.Forms.MenuItem closeItem;

    }
}

