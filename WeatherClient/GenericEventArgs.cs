﻿using System;

namespace WeatherClient
{
    public class GenericEventArgs<T> : EventArgs
    {
        public T results;
    }
}
