﻿using System;
using System.Windows.Forms;
using System.Reflection;
using Microsoft.WindowsCE.Forms;

namespace WeatherClient
{
    public partial class SettingsForm : Form
    {
        public Enum[] EnumToArray(Enum enumeration)
        {
            Type et = enumeration.GetType();

            //get the public static fields (members of the enum)
            System.Reflection.FieldInfo[] fi = et.GetFields(BindingFlags.Static | BindingFlags.Public);

            //create a new enum array
            Enum[] values = new Enum[fi.Length];

            //populate with the values
            for (int iEnum = 0; iEnum < fi.Length; iEnum++)
            {
                values[iEnum] = (Enum)fi[iEnum].GetValue(enumeration);
            }

            //return the array
            return values;
        }
        public const int CB_SHOWDROPDOWN = 0x14F;
        public static void SetDroppedDown(ComboBox comboBox)
        {
            Message comboBoxDroppedMsg = Message.Create(comboBox.Handle, CB_SHOWDROPDOWN, (IntPtr)0, IntPtr.Zero);

            MessageWindow.SendMessage(ref comboBoxDroppedMsg);
        }
        public SettingsForm()
        {
            InitializeComponent();
            var platform = System.Environment.OSVersion.Platform;
            if (platform == PlatformID.WinCE)//silly workaround for bug in dotnet cf GDI
            {
                SetDroppedDown(updateTimeComboBox);
                SetDroppedDown(metricSystemComboBox);
            }
            else
            {
                updateTimeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
                metricSystemComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            updateCheckBox.Checked = Settings.Instance.UpdateAutomatically;
            for(int i=0; i<24;i++)
                updateTimeComboBox.Items.Add(string.Format("{0} hours",i));
            updateTimeComboBox.SelectedIndex = Settings.Instance.UpdateDelayInHours - 1;
            metricSystemComboBox.DataSource = EnumToArray(Settings.Instance.UnitSystem);
            metricSystemComboBox.SelectedItem = Settings.Instance.UnitSystem;
        }
        private void menuItem1_Click(object sender, EventArgs e)
        {
            Settings.Instance.UpdateAutomatically = updateCheckBox.Checked;
            Settings.Instance.UpdateDelayInHours = updateTimeComboBox.SelectedIndex+1;
            
            try {
                Nullable<UnitSystem> unit;
                unit = Enum.Parse(typeof(UnitSystem), metricSystemComboBox.SelectedValue.ToString(),true) as Nullable<UnitSystem>;
            if (unit != null)
                Settings.Instance.UnitSystem = (UnitSystem)unit;
            }
            catch(Exception){}
            this.Close();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void updateCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            updateTimeComboBox.Enabled = updateCheckBox.Checked;
        }
    }
}