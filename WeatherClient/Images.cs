﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace WeatherClient
{
    public class Images //lazy load images
    {
        private const string folder = "icons";
        private static Dictionary<string, Image> lookup = new Dictionary<string, Image>();
        public static Image get(string code)
        {
            if (lookup.ContainsKey(code))
                return lookup[code];
            try
            {
                Image temp =  WeatherClient.Properties.Resources.ResourceManager.GetObject(String.Format("_{0}",code)) as Image;
                if(temp!=null){
                lookup.Add(code, temp);
                return temp;}
            }
            catch (Exception e) {
                Debug.WriteLine(e.GetType().ToString());
            }
            return null;
        }
    }
}
