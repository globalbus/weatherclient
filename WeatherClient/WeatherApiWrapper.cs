﻿using System;
using System.Net;
using System.IO;
using WeatherClient.SerializationObjects;
using System.Threading;
using System.Diagnostics;

namespace WeatherClient
{
    public enum UnitSystem
    {
        metric, imperial
    }
    class WeatherApiWrapper
    {
        public event EventHandler<GenericEventArgs<SearchResults>> resultsAquired;
        private Thread running;
        private static WeatherApiWrapper instance;
        public static WeatherApiWrapper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WeatherApiWrapper();
                }
                return instance;
            }
        }
        public void searchAsync(string phase)
        {
            if (running != null)
                running.Abort();
            ThreadStart starter = () => search(phase);
            running = new Thread(starter) { IsBackground = true };
            running.Start();
        }
        public void search(string phase)
        {
            string url = String.Format("http://api.openweathermap.org/data/2.5/find?q={0}&mode=xml&units={1}&lang={2}&type=accurate", phase, Settings.Instance.UnitSystem, "pl");
            Uri uri = new Uri(url);
            SearchResults results = queryObject<SearchResults>(uri);
            if (results != null)
            {
                var temp = new GenericEventArgs<SearchResults>();
                temp.results = results;
                if (resultsAquired != null)
                    resultsAquired(this, temp);
            }
        }
        public void forecast(CityAndForecast cityAndForecast)
        {
            string url = String.Format("http://api.openweathermap.org/data/2.5/forecast?id={0}&mode=xml&units={1}&lang={2}", cityAndForecast.city.Id, Settings.Instance.UnitSystem, "pl");
            Uri uri = new Uri(url);
            ForecastResults results = queryObject<ForecastResults>(uri);
            if (results != null)
            {
                cityAndForecast.Forecast = results;
                cityAndForecast.lastUpdate = DateTime.Now;
            }
        }
        private T queryObject<T>(Uri uri) where T : class{
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            T results = null;
            try
            {
                using (WebResponse response = request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                {
                    results = Serializer.deserialize<T>(stream);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.GetType().ToString());
                //MessageBox.Show(e.Message);
            }
            return results;
        }

        internal void forecastAsync(CityAndForecast cityAndForecast)
        {
            if (running != null)
                running.Abort();
            ThreadStart starter = () => forecast(cityAndForecast);
            running = new Thread(starter) { IsBackground = true };
            running.Start();
        }
    }
}
