﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using WeatherClient.SerializationObjects;
using System.Reflection;

namespace WeatherClient
{
    [Serializable]
    public class Settings
    {

        private static Settings instance;
        static Settings()
        {
            var platform = System.Environment.OSVersion.Platform;
            if (platform == PlatformID.WinCE)//silly workaround for bug in dotnet cf
            {
                savestring = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\" + filename;
            }
            else
                savestring = filename;
        }
        private Settings()
        {
            timer = new Timers();
            timer.timerCallback = timerCallback;
            timer.factor = this;
        }
        private UnitSystem unitsystem= UnitSystem.metric;
        public UnitSystem UnitSystem
        {
            get { return unitsystem; }
            set
            {
                unitsystem=value;
                if (updateAutomatically)
                    setTimer(true);//set new timer
                else
                    timer.Dispose();
            }
        }
        #region timer
        [XmlIgnore]
        private Timers timer;
        private void timerCallback(object obj)
        {
            foreach (var item in cities)
            {
                WeatherApiWrapper.Instance.forecast(item);//callback is called from pool thread, so do it synchronically
            }
        }
        void setTimer(bool now)
        {
            TimeSpan first;
            if (now)
                first = new TimeSpan(0);
            else
            {
                if (citiesCount() > 0)//if we have any cities
                    first = DateTime.Now - citiesMin();
                else
                    return;
                if (first > UpdateDelay || first.TotalHours > 24.0)//if smaller than updateDelay
                    first = new TimeSpan(0);//first run intermediately
                else
                    first = UpdateDelay - first;//first run - time from lastupdate + delay
            }
            timer.setTimer(first);
        }
        private TimeSpan updateDelay = new TimeSpan(1, 0, 0);//1h by default
        public int UpdateDelayInHours//property in hours
        {
            get { return Convert.ToInt32(updateDelay.TotalHours); }
            set
            {
                updateDelay = new TimeSpan(value, 0, 0);
                if (updateAutomatically)
                    setTimer(false);//set new timer
                else
                    timer.Dispose();
            }
        }
        public TimeSpan UpdateDelay//property in hours
        {
            get { return updateDelay; }
        }
        private bool updateAutomatically = true;//true by default
        [XmlAttribute]
        public bool UpdateAutomatically
        {
            get { return updateAutomatically; }
            set
            {
                updateAutomatically = value;
                //if (value && (!updateAutomatically))
                //{
                //    setTimer();
                //}
                //else if ((!value) && updateAutomatically)
                //{
                //    timer.Dispose();
                //}
            }
        }
        #endregion
        #region cities
        private LinkedList<CityAndForecast> cities = new LinkedList<CityAndForecast>();
        [XmlArray]//Xml only array
        public CityAndForecast[] Cities { get { return cities.ToArray(); } set { cities = new LinkedList<CityAndForecast>(value); } }
        public void addCity(City value)
        {
            var tuple = new CityAndForecast();
            tuple.city = value;
            cities.AddLast(tuple);
            var temp = new GenericEventArgs<CityAndForecast>();
            temp.results = tuple;
            if (cityAdd != null)
                cityAdd(this, temp);
            if (updateAutomatically)
            {
                setTimer(true);//set timer - update all async
            }
        }

        public void removeCity(CityAndForecast value)
        {
            cities.Remove(value);
            var temp = new GenericEventArgs<CityAndForecast>();
            temp.results = value;
            if (cityRemove != null)
                cityRemove(this, temp);
        }
        public int citiesCount()
        {
            return cities.Count();
        }


        public DateTime citiesMin()
        {
            return cities.Min(x => x.lastUpdate);
        }
        #endregion
        #region serialization
        private const string filename = "state.xml";//xml data
        private static string savestring;
        public static Settings Instance
        {
            get
            {
                if (instance == null)
                {
                    try //acquire program state from file
                    {
                        using (var reader = new FileStream(savestring, FileMode.Open))
                        {
                            instance = Serializer.deserialize<Settings>(reader);
                            instance.setTimer(false);
                        }
                    }
                    catch (Exception) { }
                    if (instance == null)//if error, generate empty
                        instance = new Settings();
                }
                return instance;
            }
        }
        public void save()
        {
            using (var writer = new FileStream(savestring, FileMode.OpenOrCreate))
            {
                Serializer.serialize<Settings>(writer, this);
                writer.SetLength(writer.Position);
            }
        }
        #endregion
        #region guiSupport
        public event EventHandler<GenericEventArgs<CityAndForecast>> cityAdd;
        public event EventHandler<GenericEventArgs<CityAndForecast>> cityRemove;

        public CityAndForecast getNext(CityAndForecast currentCity)//getNext in cyclic way
        {
            if (currentCity != null)
            {
                var el = cities.Find(currentCity);
                if (el != null && el.Next != null)
                    return el.Next.Value;
            }
            if (cities.First != null)
                return cities.First.Value;
            else
                return null;
        }
        public CityAndForecast getPrevious(CityAndForecast currentCity)
        {
            if (currentCity != null)
            {
                var el = cities.Find(currentCity);
                if (el != null && el.Previous != null)
                    return el.Previous.Value;
            }
            if (cities.First != null)
                return cities.Last.Value;
            else
                return null;
        }

        #endregion
    }
}
