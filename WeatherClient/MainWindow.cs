﻿using System;
using System.Windows.Forms;
using WeatherClient.SerializationObjects;

namespace WeatherClient
{
    public partial class MainWindow : Form
    {
        private Form searchForm;
        private Form settingsForm;
        private CityAndForecast currentCity;
        //private WeatherPanel mainPanel;
        public Form SearchForm
        {
            get
            {
                if (searchForm == null||searchForm.IsDisposed)
                    searchForm = new SearchForm();
                return searchForm;
            }
        }
        public Form SettingsForm
        {
            get
            {
                if (settingsForm == null || settingsForm.IsDisposed)
                    settingsForm = new SettingsForm();
                return settingsForm;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            this.Text = "No City";
            this.KeyUp += new KeyEventHandler(panel_KeyUp);
            RemoveMenuItem.Enabled = false;
            leftSoftButton.Enabled = false;
            Settings.Instance.cityAdd += new EventHandler<GenericEventArgs<CityAndForecast>>(Instance_cityAdd);
            Settings.Instance.cityRemove += new EventHandler<GenericEventArgs<CityAndForecast>>(Instance_cityRemove);
            currentCityChanged(Settings.Instance.getNext(null));
        }

        void Instance_cityAdd(object sender, GenericEventArgs<CityAndForecast> e)
        {
            if (currentCity == null){
                currentCityChanged(e.results);
            }
        }
        void Instance_cityRemove(object sender, GenericEventArgs<CityAndForecast> e)
        {
            if (currentCity == e.results)
            {
                //move to next
                var next = Settings.Instance.getPrevious(currentCity);
                if (next != null)
                {
                    currentCityChanged(next);
                    return;
                }
                next = Settings.Instance.getNext(currentCity);
                if (next != null)
                {
                    currentCityChanged(next);
                    return;
                }
                currentCity = null;
                RemoveMenuItem.Enabled = false;
                leftSoftButton.Enabled = false;
                this.Text = "No City";
                mainPanel.Data = null;
            }
        }

        private void addNew_Click(object sender, EventArgs e)
        {
            SearchForm.Show();
        }

        private void update_Click(object sender, EventArgs e)
        {
            WeatherApiWrapper.Instance.forecastAsync(this.currentCity);
        }

        private void removeCurrent_Click(object sender, EventArgs e)
        {
            Settings.Instance.removeCity(currentCity);
        }

        private void openSettings_Click(object sender, EventArgs e)
        {
            SettingsForm.Show();
        }
        private void panel_KeyUp(object sender, KeyEventArgs e)
        {
            if(currentCity!=null)
            if (e.KeyCode == Keys.Left)
            {
                currentCityChanged(Settings.Instance.getPrevious(currentCity));
            }
            else if (e.KeyCode == Keys.Right)
            {
                currentCityChanged(Settings.Instance.getNext(currentCity));
            }
        }

        private void currentCityChanged(CityAndForecast cityAndForecast)
        {
            if (cityAndForecast != null)
            {
                currentCity = cityAndForecast;
                mainPanel.Data = cityAndForecast;
                this.Text = currentCity.ToString();
                RemoveMenuItem.Enabled = true;
                leftSoftButton.Enabled = true;
            }
        }
        private void MainWindow_Closed(object sender, EventArgs e)
        {
            Settings.Instance.save();
        }

        private void closeItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}