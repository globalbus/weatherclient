﻿using System;
using System.Threading;

namespace WeatherClient
{
    public class Timers : IDisposable
    {
        private Timer timer;//autoupdate timer
        public TimerCallback timerCallback;
        public Settings factor;
        public void setTimer(TimeSpan time)
        {
            if (timer != null)
                timer.Dispose();//if there is already a timer
            timer = new Timer(timerCallback, null, time, factor.UpdateDelay);
        }


        public void Dispose()
        {
            timer.Dispose();
        }
    }
}
