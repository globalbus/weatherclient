﻿using System.IO;
using System.Xml.Serialization;

namespace WeatherClient
{
    class Serializer
    {
        public static T deserialize<T>(Stream stream) where T : class
        {
            T results = default(T);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            results = serializer.Deserialize(stream) as T;
            return results;
        }
        public static void serialize<T>(Stream stream, T obj) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(stream, obj);
        }
    }
}
