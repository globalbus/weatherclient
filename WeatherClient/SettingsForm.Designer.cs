﻿namespace WeatherClient
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.updateCheckBox = new System.Windows.Forms.CheckBox();
            this.updateTimeComboBox = new System.Windows.Forms.ComboBox();
            this.metricSystemLabel = new System.Windows.Forms.Label();
            this.metricSystemComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "OK";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Cancel";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // updateCheckBox
            // 
            this.updateCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateCheckBox.Location = new System.Drawing.Point(0, 0);
            this.updateCheckBox.Name = "updateCheckBox";
            this.updateCheckBox.Size = new System.Drawing.Size(320, 26);
            this.updateCheckBox.TabIndex = 0;
            this.updateCheckBox.Text = "Auto Update";
            this.updateCheckBox.CheckStateChanged += new System.EventHandler(this.updateCheckBox_CheckStateChanged);
            // 
            // updateTimeComboBox
            // 
            this.updateTimeComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateTimeComboBox.Items.Add("1 hour");
            this.updateTimeComboBox.Items.Add("2 hours");
            this.updateTimeComboBox.Items.Add("3 hours");
            this.updateTimeComboBox.Items.Add("6 hours");
            this.updateTimeComboBox.Items.Add("24 hours");
            this.updateTimeComboBox.Location = new System.Drawing.Point(0, 26);
            this.updateTimeComboBox.Name = "updateTimeComboBox";
            this.updateTimeComboBox.Size = new System.Drawing.Size(320, 24);
            this.updateTimeComboBox.TabIndex = 1;
            // 
            // metricSystemLabel
            // 
            this.metricSystemLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.metricSystemLabel.Location = new System.Drawing.Point(0, 50);
            this.metricSystemLabel.Name = "metricSystemLabel";
            this.metricSystemLabel.Size = new System.Drawing.Size(320, 30);
            this.metricSystemLabel.Text = "Metric System";
            // 
            // metricSystemComboBox
            // 
            this.metricSystemComboBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.metricSystemComboBox.Location = new System.Drawing.Point(0, 80);
            this.metricSystemComboBox.Name = "metricSystemComboBox";
            this.metricSystemComboBox.Size = new System.Drawing.Size(320, 30);
            this.metricSystemComboBox.TabIndex = 4;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(131F, 131F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(320, 186);
            this.Controls.Add(this.metricSystemComboBox);
            this.Controls.Add(this.metricSystemLabel);
            this.Controls.Add(this.updateTimeComboBox);
            this.Controls.Add(this.updateCheckBox);
            this.Menu = this.mainMenu1;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.CheckBox updateCheckBox;
        private System.Windows.Forms.ComboBox updateTimeComboBox;
        private System.Windows.Forms.Label metricSystemLabel;
        private System.Windows.Forms.ComboBox metricSystemComboBox;
    }
}